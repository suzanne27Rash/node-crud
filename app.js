const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();

let port = 8080;

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + '/app/index.html'));
})

app.post('/quotes', (req, res) => {
    res.send('quotes page');
    // console.log('Hellllloooooooo!');
})

app.get('/test', (req, res) => {
    res.send('test page');
})

app.listen(port, () => {
    console.log(`Server is up and running on port number ${port}`);
});

